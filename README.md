This program is the most simple example for demonstrating the usage of JNI for accessing Java functions from within C++ code. It uses Qt Framework's QAndroidJniObject class to ease things up. It displays a Toast message when the only button on the screen is pressed.

Visit my website below for more tutorials and examples about C++, Qt, OpenCV and Android Development.
http://www.amin-ahmadi.com