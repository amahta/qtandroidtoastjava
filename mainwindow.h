#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/*
www.amin-ahmadi.com
*/

#include <QMainWindow>
#include <QtAndroidExtras>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_pressed();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
