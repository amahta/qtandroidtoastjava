#include "mainwindow.h"
#include "ui_mainwindow.h"

/*
www.amin-ahmadi.com
*/

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_pressed()
{
    QAndroidJniObject tstMsg = QAndroidJniObject::fromString("Hello world! :)");
    QAndroidJniObject::callStaticMethod<void>("com/amin/QtAndroidToastJava/QtAndroidToastJava",
                                              "sMakeToast",
                                              "(Ljava/lang/String;)V",
                                              tstMsg.object<jstring>());
}
